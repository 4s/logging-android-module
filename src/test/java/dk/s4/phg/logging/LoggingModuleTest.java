package dk.s4.phg.logging;

import android.app.Activity;
import android.util.Log;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.concurrent.atomic.AtomicReference;

import dk.s4.phg.baseplate.ApplicationState;
import dk.s4.phg.baseplate.Context;
import dk.s4.phg.baseplate.CoreError;
import dk.s4.phg.baseplate.LogCollectorModule;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest(Log.class)
public class LoggingModuleTest {

    private LoggingModule loggingModule;
    private AtomicReference<LoggedMessage> logMessage;

    @Before
    public void setup() {
        PowerMockito.mockStatic(Log.class);
        logMessage = new AtomicReference<>();
        when(Log.println(anyInt(), any(), any())).thenAnswer(invocation -> {
            logMessage.set(LoggedMessage.from(invocation.getArguments()));
            return 42;
        });
        loggingModule = new LoggingModule(new MockContext(), mock(Activity.class));
    }

    @Test
    public void isInitialized() {
        assertThat(loggingModule).isNotNull();
        assertThat(loggingModule.getApplicationState()).isEqualTo(ApplicationState.INITIALIZING);
    }

    @Test
    public void logDebugMessage() {
        LogCollectorModule.LogMessage message = mock(LogCollectorModule.LogMessage.class);
        message.level = LogCollectorModule.Level.DEBUG;
        message.moduleTag = "moduleTag";
        message.message = "abc";

        loggingModule.log(message);

        assertThat(logMessage.get().moduleBase).isEqualTo(message.moduleTag);
        assertThat(logMessage.get().priority).isEqualTo(Log.DEBUG);
        assertThat(logMessage.get().message).contains("DEBUG");
        assertThat(logMessage.get().message).contains(message.message);
        assertThat(logMessage.get().message).contains(message.moduleTag);
    }

    @Test
    public void logErrorMessage() {
        LogCollectorModule.LogMessage message = mock(LogCollectorModule.LogMessage.class);
        message.level = LogCollectorModule.Level.ERROR;
        message.moduleTag = "moduleTag";
        message.message = "abc";

        loggingModule.log(message);

        assertThat(logMessage.get().moduleBase).isEqualTo(message.moduleTag);
        assertThat(logMessage.get().priority).isEqualTo(Log.ERROR);
        assertThat(logMessage.get().message).contains("ERROR");
        assertThat(logMessage.get().message).contains(message.message);
        assertThat(logMessage.get().message).contains(message.moduleTag);
    }


    @Test
    public void logFatalMessage() {
        LogCollectorModule.LogMessage message = mock(LogCollectorModule.LogMessage.class);
        message.level = LogCollectorModule.Level.FATAL;
        message.moduleTag = "moduleTag";
        message.message = "abc";

        loggingModule.log(message);

        assertThat(logMessage.get().moduleBase).isEqualTo(message.moduleTag);
        assertThat(logMessage.get().priority).isEqualTo(Log.ASSERT);
        assertThat(logMessage.get().message).contains("FATAL");
        assertThat(logMessage.get().message).contains(message.message);
        assertThat(logMessage.get().message).contains(message.moduleTag);
    }


    @Test
    public void logInfoMessage() {
        LogCollectorModule.LogMessage message = mock(LogCollectorModule.LogMessage.class);
        message.level = LogCollectorModule.Level.INFO;
        message.moduleTag = "moduleTag";
        message.message = "abc";

        loggingModule.log(message);

        assertThat(logMessage.get().moduleBase).isEqualTo(message.moduleTag);
        assertThat(logMessage.get().priority).isEqualTo(Log.INFO);
        assertThat(logMessage.get().message).contains("INFO");
        assertThat(logMessage.get().message).contains(message.message);
        assertThat(logMessage.get().message).contains(message.moduleTag);
    }

    @Test
    public void logWarnMessage() {
        LogCollectorModule.LogMessage message = mock(LogCollectorModule.LogMessage.class);
        message.level = LogCollectorModule.Level.WARN;
        message.moduleTag = "moduleTag";
        message.message = "abc";

        loggingModule.log(message);

        assertThat(logMessage.get().moduleBase).isEqualTo(message.moduleTag);
        assertThat(logMessage.get().priority).isEqualTo(Log.WARN);
        assertThat(logMessage.get().message).contains("WARN");
        assertThat(logMessage.get().message).contains(message.message);
        assertThat(logMessage.get().message).contains(message.moduleTag);
    }

    @Test
    public void logWithFileName() {
        LogCollectorModule.LogMessage message = mock(LogCollectorModule.LogMessage.class);
        message.level = LogCollectorModule.Level.INFO;
        message.moduleTag = "moduleTag";
        message.message = "abc";
        message.fileName = "somefile.class";
        message.lineNumber = 42;

        loggingModule.log(message);

        assertThat(logMessage.get().message).contains(message.fileName);
        assertThat(logMessage.get().message).contains("" + message.lineNumber);
    }

    @Test
    public void logWithClassName() {
        LogCollectorModule.LogMessage message = mock(LogCollectorModule.LogMessage.class);
        message.level = LogCollectorModule.Level.INFO;
        message.moduleTag = "moduleTag";
        message.message = "abc";
        message.className = "someClass";

        loggingModule.log(message);

        assertThat(logMessage.get().message).contains(message.className);
    }

    @Test
    public void logWithCausedBy() {
        LogCollectorModule.LogMessage message = mock(LogCollectorModule.LogMessage.class);
        message.level = LogCollectorModule.Level.INFO;
        message.moduleTag = "moduleTag";
        message.message = "abc";
        message.causedBy = new CoreError("submoduleTag", "coreError message");

        loggingModule.log(message);

        assertThat(logMessage.get().message).contains("Caused by");
        assertThat(logMessage.get().message).contains("submoduleTag");
        assertThat(logMessage.get().message).contains("coreError message");
    }

    @Test
    public void logWithFunctionName() {
        LogCollectorModule.LogMessage message = mock(LogCollectorModule.LogMessage.class);
        message.level = LogCollectorModule.Level.INFO;
        message.moduleTag = "moduleTag";
        message.message = "abc";
        message.functionName = "SomeFunction";

        loggingModule.log(message);

        assertThat(logMessage.get().message).contains(message.functionName);
    }

    @Test
    public void logWithFunctionNameAndClassName() {
        LogCollectorModule.LogMessage message = mock(LogCollectorModule.LogMessage.class);
        message.level = LogCollectorModule.Level.INFO;
        message.moduleTag = "moduleTag";
        message.message = "abc";
        message.functionName = "SomeFunction";
        message.className = "SomeClass";

        loggingModule.log(message);

        assertThat(logMessage.get().message).contains(message.functionName);
        assertThat(logMessage.get().message).contains(message.className);
    }

    class MockContext extends Context {
        MockContext() {
            super(42);
        }

        @Override
        protected void initialize() {

        }
    }

    static class LoggedMessage {
        private final int priority;
        private final String moduleBase;
        private final String message;

        private LoggedMessage(int priority, String moduleBase, String message) {
            this.priority = priority;
            this.moduleBase = moduleBase;
            this.message = message;
        }

        public static LoggedMessage from(Object[] args) {
            return new LoggedMessage((Integer) args[0], (String) args[1], (String) args[2]);
        }
    }
}