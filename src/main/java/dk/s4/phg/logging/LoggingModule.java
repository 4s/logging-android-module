package dk.s4.phg.logging;

import android.app.Activity;
import android.util.Log;

import dk.s4.phg.baseplate.Context;
import dk.s4.phg.baseplate.LogCollectorModule;




/**
 * Simple log collector printing to android.util.log.
 *
 * This log collector module will pretty-print log messages to
 * android.util.Log.println() using ANSI colour and effect codes.
 *
 * @author <a href="mailto:jacob.andersen@alexandra.dk">Jacob
 *         Andersen</a>, The Alexandra Institute.
 *
 * @copyright &copy; 2019 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */
public class LoggingModule extends LogCollectorModule {

    /**
     * Create a simple log collector module, printing log messages to
     * the android logger.
     *
     * @param context      The PHG Core common Context object shared by
     *                     all modules connected to the JVM baseplate
     * @param appActivity  The Android activity
     */
    public LoggingModule(Context context, Activity appActivity) {
        super(context, "Logging");
        // Default is all logging output - the application may choose
        // to change this
        setLogCollectingLevel(Level.DEBUG);
        // Start the module
        start();
    }

    /**
     * Pretty-print the log message to an ANSI terminal
     * @param msg The log message to print
     */
    protected void log(LogMessage msg) {

        // We build the output one element at the time in a
        // StringBuffer
        StringBuilder sb = new StringBuilder();

        // Keeping track on message effects used for FATAL and ERROR
        // levels
        String messageEffect = "";

        // android.util.Log priority
        int priority;

        // Debug level
        switch (msg.level) {
        case DEBUG:
            priority = Log.DEBUG;
            sb.append(Effect.BOLD);
            sb.append("DEBUG");
            break;
        case INFO:
            priority = Log.INFO;
            sb.append(Effect.BLUE_BRIGHT);
            sb.append(Effect.BOLD);
            sb.append("INFO");
            break;
        case WARN:
            priority = Log.WARN;
            sb.append(Effect.YELLOW);
            sb.append(Effect.BOLD);
            sb.append("WARNING");
            break;
        case ERROR:
            priority = Log.ERROR;
            sb.append(Effect.RED);
            sb.append(Effect.BOLD);
            sb.append("ERROR");
            messageEffect = Effect.BOLD.toString();
            break;
        default: // FATAL
            priority = Log.ASSERT;
            sb.append(Effect.RED_BRIGHT);
            sb.append(Effect.BOLD);
            sb.append("FATAL");
            messageEffect = Effect.RED.toString() + Effect.BOLD.toString();
            break;
        }
        sb.append(Effect.RESET);

        // Module TAG
        sb.append(messageEffect + " in " + msg.moduleTag);

        // File name and line number
        if (msg.fileName != null && !msg.fileName.isEmpty()) {
            sb.append(" (");
            sb.append(Effect.UNDERLINE);
            sb.append(msg.fileName + ":" + msg.lineNumber);
            sb.append(Effect.RESET);
            sb.append(messageEffect + ")");
        }

        // Class name
        if (msg.className != null && !msg.className.isEmpty()) {
            sb.append(" " + msg.className);
        }

        // Function name
        if (msg.functionName != null && !msg.functionName.isEmpty()) {
            if (msg.className != null && !msg.className.isEmpty()) {
                sb.append("::");
            } else {
                sb.append(" ");
            }
            sb.append(msg.functionName);
        }

        // Message
        sb.append(Effect.RESET + "\n" + messageEffect + "    " + msg.message);

        // CausedBy
        if (msg.causedBy != null) {
            sb.append(Effect.RESET + "\n" + messageEffect + "    Caused by:"
                      + Effect.RESET + "\n");
            sb.append(msg.causedBy);
        } else {
            sb.append(Effect.RESET);
        }

        // The message is printed on android.util.Log.println()
        Log.println(priority, msg.moduleTag, sb.toString());
    }



    /**
     * ANSI terminal escape sequences used to add colour and effects.
     */
    protected enum Effect {
        RESET("\033[0m"),

        BLACK("\033[0;30m"),
        RED("\033[0;31m"),
        GREEN("\033[0;32m"),
        YELLOW("\033[0;33m"),
        BLUE("\033[0;34m"),

        BOLD("\033[1m"),
        UNDERLINE("\033[4m"),

        BLACK_BRIGHT("\033[0;90m"),
        RED_BRIGHT("\033[0;91m"),
        GREEN_BRIGHT("\033[0;92m"),
        YELLOW_BRIGHT("\033[0;93m"),
        BLUE_BRIGHT("\033[0;94m");

        private final String ansiSequence;

        Effect(String ansiSequence) { this.ansiSequence = ansiSequence; }

        @Override
        public String toString() {
            return ansiSequence;
        }
    }
}
